# Clean /boot/ for unused kernels
sudo dpkg --list 'linux-image*'|awk '{ if ($1=="ii") print $2}'|grep -v `uname -r` | xargs sudo apt-get purge $1 -y
